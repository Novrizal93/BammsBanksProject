package project.rhiedzal.bammsbankproject.MANAGER;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import project.rhiedzal.bammsbankproject.R;

public class ManagerLogin extends AppCompatActivity {

    @BindView(R.id.username) EditText et_username;
    @BindView(R.id.password) EditText et_password;
    @BindView(R.id.btn_login) Button btn_login;

    String username, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manager_login);
        ButterKnife.bind(this);

        click_action();
    }

    public void click_action(){
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                username = et_username.getText().toString();
                password = et_password.getText().toString();

                if(TextUtils.isEmpty(username)){
                    et_username.setError("Can't be empty");
                }else if(TextUtils.isEmpty(password)){
                    et_password.setError("Can't be empty");
                }else{
                    new LoginCheck(ManagerLogin.this.getBaseContext()).execute("");
                }
            }
        });
    }

    @SuppressLint("StaticFieldLeak")
    private class LoginCheck extends AsyncTask<String, String, String> {

        Context context;
        String code, message, excptn;

        LoginCheck(Context context)
        {
            this.context =  context;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {

            try {
                InputStream is = getAssets().open("JsonLoginManager");
                int size = is.available();
                byte[] buffer = new byte[size];
                is.read(buffer);
                is.close();

                JSONObject json = new JSONObject(new String(buffer));

                if(json.has("code")){
                    code = json.getString("code");
                }
                if(json.has("message")){
                    message = json.getString("message");
                }

                excptn = "good";
            } catch (IOException e) {
                e.printStackTrace();
                excptn = "error";
            } catch (JSONException e) {
                e.printStackTrace();
                excptn = "error";
            } catch (NullPointerException e){
                e.printStackTrace();
                excptn = "error";
            }
            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(excptn.equals("error")){
                new MaterialDialog.Builder(ManagerLogin.this)
                        .title("Internal Error!")
                        .content("Internal Error, Please try again")
                        .show();
            }else {
                if (!code.equals("200")) {
                    new MaterialDialog.Builder(ManagerLogin.this)
                            .title("Sorry!")
                            .content(message)
                            .show();
                } else {
                    Intent i = new Intent(ManagerLogin.this, ManagerHome.class);
                    startActivity(i);
                }
            }
        }
    }
}
