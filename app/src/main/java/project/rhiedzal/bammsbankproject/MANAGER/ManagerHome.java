package project.rhiedzal.bammsbankproject.MANAGER;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;


import butterknife.BindView;
import butterknife.ButterKnife;
import project.rhiedzal.bammsbankproject.Home;
import project.rhiedzal.bammsbankproject.METHOD.Method;
import project.rhiedzal.bammsbankproject.R;
import project.rhiedzal.bammsbankproject.SQL.SQLHelper;

public class ManagerHome extends AppCompatActivity {

    @BindView(R.id.money) TextView tv_money;
    @BindView(R.id.total_client) TextView tv_total_client;
    @BindView(R.id.total_deposit) TextView tv_total_deposit;
    @BindView(R.id.total_withdraw) TextView tv_total_withdraw;
    @BindView(R.id.total_amount_before) TextView tv_total_amount_before;

    Cursor cursor;
    SQLHelper sqlHelper;
    SQLiteDatabase sqLiteDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manager_home);
        ButterKnife.bind(this);

        declare_function();
    }

    public void declare_function(){
        sqlHelper = new SQLHelper(this);
        sqLiteDatabase = sqlHelper.getReadableDatabase();

        new getData(ManagerHome.this.getBaseContext()).execute("");
    }

    @SuppressLint("StaticFieldLeak")
    private class getData extends AsyncTask<String, String, String> {

        Context context;
        String money = "0", excptn;
        int m, total_withdraw = 0, total_deposit = 0;

        getData(Context context)
        {
            this.context =  context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {

            try {

                cursor = sqLiteDatabase.rawQuery("SELECT amount FROM transaction_client WHERE type='DEPOSITE'", null);
                for(int i=0; i<cursor.getCount(); i++){
                    cursor.moveToPosition(i);
                    total_deposit += cursor.getInt(0);
                }

                cursor = sqLiteDatabase.rawQuery("SELECT amount FROM transaction_client WHERE type='WITHDRAW'", null);
                for(int i=0; i<cursor.getCount(); i++){
                    cursor.moveToPosition(i);
                    total_withdraw += cursor.getInt(0);
                }

                cursor = sqLiteDatabase.rawQuery("SELECT amount FROM account", null);
                cursor.moveToFirst();

                if(cursor.getCount() > 0) {
                    for (int i = 0; i < cursor.getCount(); i++) {
                        cursor.moveToPosition(i);
                        m = Integer.parseInt(cursor.getString(0)) + m;
                    }

                    money = String.valueOf(m);
                    cursor.moveToFirst();
                }else{
                    money = "0";
                }

                excptn = "good";
            } catch (NullPointerException e){
                e.printStackTrace();
                excptn = "error";
            }
            return "";
        }

        @SuppressLint("SetTextI18n")
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(excptn.equals("error")){
                new MaterialDialog.Builder(ManagerHome.this)
                        .title("Internal Error!")
                        .content("Internal Error, Please try again")
                        .show();
            }else {
                tv_money.setText("Rp."+ Method.titik(money));
                tv_total_client.setText(String.valueOf(cursor.getCount()));
                tv_total_withdraw.setText("Rp."+Method.titik(String.valueOf(total_withdraw)));
                tv_total_amount_before.setText("Rp."+Method.titik(String.valueOf((Integer.parseInt(money)+total_withdraw))));
                tv_total_deposit.setText("Rp."+Method.titik(String.valueOf(total_deposit)));
            }
        }
    }

    @Override
    public void onBackPressed() {
        new MaterialDialog.Builder(this)
                .title("Logout From Bank Manager?")
                .positiveText("YES")
                .negativeText("NO")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        Intent i = new Intent(ManagerHome.this, Home.class);
                        startActivity(i);
                    }
                })
                .show();
    }
}
