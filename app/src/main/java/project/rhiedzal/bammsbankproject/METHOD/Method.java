package project.rhiedzal.bammsbankproject.METHOD;

/**
 * Created by Rhiedzal on 8/18/2018.
 */

public final class Method {
    public static String titik(String angka) {
        try {
            angka = String.format("%,d", Long.parseLong(angka.toString()));
            return angka;
        } catch (NumberFormatException e) {
            return angka;
        }
    }

    public static String space_4(String s) {
        StringBuilder sb = new StringBuilder(s);
        for (int i = sb.length() - 4; i > 0; i -= 4)
            sb.insert(i, ' ');
        return sb.toString();
    }
}
