package project.rhiedzal.bammsbankproject.CLIENT.WITHDRAW;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import project.rhiedzal.bammsbankproject.CLIENT.ClientHome;
import project.rhiedzal.bammsbankproject.CLIENT.TRANSFER.ClientTransfer;
import project.rhiedzal.bammsbankproject.METHOD.Method;
import project.rhiedzal.bammsbankproject.R;
import project.rhiedzal.bammsbankproject.SQL.SQLHelper;

public class ClientWithdraw extends AppCompatActivity {

    @BindView(R.id.account_name) TextView tv_account_name;
    @BindView(R.id.account_amount) TextView tv_account_amount;
    @BindView(R.id.account_number) TextView tv_account_number;
    @BindView(R.id.nominal) EditText et_nominal;
    @BindView(R.id.btn_process) Button btn_process;

    String customer_id, account_number, account_money, account_name, nominal_withdraw;

    Cursor cursor;
    SQLHelper sqlHelper;
    SQLiteDatabase sqLiteDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle b = getIntent().getExtras();
        customer_id = b.getString("customer_id");
        account_number = b.getString("account_number");
        account_money = b.getString("account_amount");
        account_name = b.getString("account_name");
        setContentView(R.layout.activity_client_withdraw);
        ButterKnife.bind(this);

        set_view();
        declare_function();
        click_action();
    }

    public void set_view(){
        tv_account_name.setText(account_name);
        tv_account_amount.setText("Rp"+ Method.titik(account_money));
        tv_account_number.setText(Method.space_4(account_number));
    }

    public void declare_function(){
        sqlHelper = new SQLHelper(this);
        sqLiteDatabase = sqlHelper.getReadableDatabase();
    }

    public void click_action(){
        btn_process.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nominal_withdraw = et_nominal.getText().toString();
                if(TextUtils.isEmpty(nominal_withdraw)){
                    et_nominal.setError("Can't be empty");
                }else if(Integer.parseInt(nominal_withdraw) > Integer.parseInt(account_money)){
                    new MaterialDialog.Builder(ClientWithdraw.this)
                            .title("Sorry!")
                            .content("Your withdraw is bigger than your amount, please check and try again")
                            .show();
                }else{
                    new MaterialDialog.Builder(ClientWithdraw.this)
                            .title("Input Password")
                            .inputType(InputType.TYPE_TEXT_VARIATION_PASSWORD)
                            .input("Your account password", "", new MaterialDialog.InputCallback() {
                                @Override
                                public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                                    dialog.dismiss();
                                    if(TextUtils.isEmpty(input)){
                                        new MaterialDialog.Builder(ClientWithdraw.this)
                                                .title("Sorry!")
                                                .content("Input can't be empty")
                                                .show();
                                    }else {
                                        new withdraw(ClientWithdraw.this.getBaseContext(), String.valueOf(input)).execute("");
                                    }
                                }
                            })
                            .negativeText("Cancel")
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    dialog.dismiss();
                                }
                            })
                            .show();
                }
            }
        });
    }


    @SuppressLint("StaticFieldLeak")
    private class withdraw extends AsyncTask<String, String, String> {

        Context context;
        String password, excptn, message, code;
        int money;

        withdraw(Context context, String password)
        {
            this.context =  context;
            this.password = password;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @SuppressLint("SimpleDateFormat")
        @Override
        protected String doInBackground(String... params) {

            try {
                sqLiteDatabase = sqlHelper.getReadableDatabase();
                cursor = sqLiteDatabase.rawQuery("SELECT * FROM customer WHERE customer_id="+customer_id+" AND password='"+password+"'", null);
                cursor.moveToFirst();

                if(cursor.getCount() < 1){
                    code = "500";
                    message = "Password is doesn't match";
                }else{
                    code = "200";
                    message = "OK";

                    //update amount
                    sqLiteDatabase = sqlHelper.getWritableDatabase();
                    ContentValues cv = new ContentValues();
                    cv.put("amount", String.valueOf(Integer.parseInt(account_money) - Integer.parseInt(nominal_withdraw)));
                    sqLiteDatabase.update("account", cv, "account_id="+customer_id, null);

                    //insert data transaction
                    ContentValues val_trans = new ContentValues();
                    val_trans.put("type", "WITHDRAW");
                    val_trans.put("name", account_name);
                    val_trans.put("customer_id", customer_id);
                    val_trans.put("amount", nominal_withdraw);
                    val_trans.put("date_time", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
                    sqLiteDatabase.insert("transaction_client", null, val_trans);
                }

                excptn = "good";
            } catch (NullPointerException e){
                e.printStackTrace();
                excptn = "error";
            }
            return "";
        }

        @SuppressLint("SetTextI18n")
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(excptn.equals("error")){
                new MaterialDialog.Builder(ClientWithdraw.this)
                        .title("Internal Error!")
                        .content("Internal Error, Please try again")
                        .show();
            }else {
                if(!code.equals("200")){
                    new MaterialDialog.Builder(ClientWithdraw.this)
                            .title("Sorry!")
                            .content(message)
                            .show();
                }else {
                    new MaterialDialog.Builder(ClientWithdraw.this)
                            .title("WithDraw Success")
                            .positiveText("OK")
                            .cancelable(false)
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    Intent i = new Intent(ClientWithdraw.this, ClientHome.class);
                                    startActivity(i);
                                }
                            })
                            .show();
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(ClientWithdraw.this, ClientHome.class);
        startActivity(i);
    }
}
