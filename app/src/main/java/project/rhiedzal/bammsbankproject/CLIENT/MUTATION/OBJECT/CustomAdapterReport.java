package project.rhiedzal.bammsbankproject.CLIENT.MUTATION.OBJECT;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.ButterKnife;
import project.rhiedzal.bammsbankproject.METHOD.Method;
import project.rhiedzal.bammsbankproject.R;

/**
 * Created by Rhiedzal on 8/19/2018.
 */

public class CustomAdapterReport extends ArrayAdapter<String> {
    private final Activity context;
    private final ArrayList<ObjectReport> listItem;

    public CustomAdapterReport(Activity context, ArrayList listItem) {
        super(context, R.layout.list_single_report, listItem);
        this.context = context;
        this.listItem = listItem;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View v = inflater.inflate(R.layout.list_single_report, null, true);

        TextView nominal, type, date;
        nominal = v.findViewById(R.id.nominal);
        type = v.findViewById(R.id.type);
        date = v.findViewById(R.id.date);

        nominal.setText("Rp"+ Method.titik(listItem.get(position).getAmount()));
        type.setText(listItem.get(position).getType());
        date.setText(listItem.get(position).getDatetime());

        return v;
    }
}
