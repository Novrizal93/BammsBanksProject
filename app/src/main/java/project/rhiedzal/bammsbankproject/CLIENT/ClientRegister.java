package project.rhiedzal.bammsbankproject.CLIENT;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import project.rhiedzal.bammsbankproject.R;
import project.rhiedzal.bammsbankproject.SQL.SQLHelper;

public class ClientRegister extends AppCompatActivity {

    @BindView(R.id.name) EditText et_name;
    @BindView(R.id.username) EditText et_username;
    @BindView(R.id.phone) EditText et_phone;
    @BindView(R.id.address) EditText et_address;
    @BindView(R.id.password) EditText et_password;
    @BindView(R.id.btn_register) Button btn_register;
    @BindView(R.id.login) TextView tv_login;

    SQLHelper sqlHelper;
    SQLiteDatabase sqLiteDatabase;

    String name, username, phone, address, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_register);
        ButterKnife.bind(this);

        declare_function();
        click_action();
    }

    public void declare_function(){
        sqlHelper = new SQLHelper(this);
        sqLiteDatabase = sqlHelper.getWritableDatabase();
    }

    public void click_action(){
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                name = et_name.getText().toString();
                username = et_username.getText().toString();
                phone = et_phone.getText().toString();
                address = et_address.getText().toString();
                password = et_password.getText().toString();

                if(TextUtils.isEmpty(name)){
                    et_name.setError("Can't be empty");
                }else if(TextUtils.isEmpty(username)){
                    et_username.setError("Can't be empty");
                }else if(TextUtils.isEmpty(phone)){
                    et_phone.setError("Can't be empty");
                }else if(TextUtils.isEmpty(address)){
                    et_address.setError("Can't be empty");
                }else if(TextUtils.isEmpty(password)){
                    et_password.setError("Can't be empty");
                }else{
                    new RegisterAccount(ClientRegister.this.getBaseContext()).execute("");
                }
            }
        });

        tv_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ClientRegister.this, ClientLogin.class);
                startActivity(i);
            }
        });
    }

    @SuppressLint("StaticFieldLeak")
    private class RegisterAccount extends AsyncTask<String, String, String> {

        Context context;
        String code, message, excptn;

        RegisterAccount(Context context)
        {
            this.context =  context;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {

            try {
                InputStream is = getAssets().open("JsonClientRequest");
                int size = is.available();
                byte[] buffer = new byte[size];
                is.read(buffer);
                is.close();

                JSONObject json = new JSONObject(new String(buffer));

                if(json.has("code")){
                    code = json.getString("code");
                }
                if(json.has("message")){
                    message = json.getString("message");
                }

                excptn = "good";
            } catch (IOException e) {
                e.printStackTrace();
                excptn = "error";
            } catch (JSONException e) {
                e.printStackTrace();
                excptn = "error";
            } catch (NullPointerException e){
                e.printStackTrace();
                excptn = "error";
            }
            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(excptn.equals("error")){
                new MaterialDialog.Builder(ClientRegister.this)
                        .title("Internal Error!")
                        .content("Internal Error, Please try again")
                        .show();
            }else {
                if (!code.equals("200")) {
                    new MaterialDialog.Builder(ClientRegister.this)
                            .title("Sorry!")
                            .content(message)
                            .show();
                } else {
                    insert_db();

                    new MaterialDialog.Builder(ClientRegister.this)
                            .title("Register Success")
                            .content(message)
                            .positiveText("LOGIN")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    Intent i = new Intent(ClientRegister.this, ClientLogin.class);
                                    startActivity(i);
                                }
                            })
                            .cancelable(false)
                            .show();
                }
            }
        }
    }

    public void insert_db(){
        ContentValues val_cust = new ContentValues();
        val_cust.put("name", name);
        val_cust.put("phone", phone);
        val_cust.put("address", address);
        val_cust.put("username", username);
        val_cust.put("password", password);

        ContentValues val_acc = new ContentValues();
        val_acc.put("account_number", String.valueOf(System.currentTimeMillis()));
        val_acc.put("type", "CUSTOMER");
        val_acc.put("amount", "0");

        sqLiteDatabase.insert("customer", null, val_cust);
        sqLiteDatabase.insert("account", null, val_acc);
    }
}
