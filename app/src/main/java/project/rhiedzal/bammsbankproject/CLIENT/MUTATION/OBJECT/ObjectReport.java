package project.rhiedzal.bammsbankproject.CLIENT.MUTATION.OBJECT;

/**
 * Created by Rhiedzal on 8/19/2018.
 */

public class ObjectReport {

    String amount, datetime, type;

    public ObjectReport(String amount, String datetime, String type) {
        this.amount = amount;
        this.datetime = datetime;
        this.type = type;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
