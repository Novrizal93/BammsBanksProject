package project.rhiedzal.bammsbankproject.CLIENT.MUTATION;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import project.rhiedzal.bammsbankproject.CLIENT.ClientHome;
import project.rhiedzal.bammsbankproject.CLIENT.MUTATION.OBJECT.CustomAdapterReport;
import project.rhiedzal.bammsbankproject.CLIENT.MUTATION.OBJECT.ObjectReport;
import project.rhiedzal.bammsbankproject.METHOD.Method;
import project.rhiedzal.bammsbankproject.R;
import project.rhiedzal.bammsbankproject.SQL.SQLHelper;

public class ClientMutation extends AppCompatActivity {

    @BindView(R.id.account_name) TextView tv_account_name;
    @BindView(R.id.account_number) TextView tv_account_number;
    @BindView(R.id.list_report) ListView lv_report;

    String customer_id, account_number, account_money, account_name;

    ArrayList<ObjectReport> objectReports = new ArrayList<>();
    CustomAdapterReport customAdapterReport;

    Cursor cursor;
    SQLHelper sqlHelper;
    SQLiteDatabase sqLiteDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle b = getIntent().getExtras();
        customer_id = b.getString("customer_id");
        account_number = b.getString("account_number");
        account_money = b.getString("account_money");
        account_name = b.getString("account_name");
        setContentView(R.layout.activity_client_mutation);
        ButterKnife.bind(this);

        declare_function();
        set_adapter();
    }

    public void declare_function(){
        sqlHelper = new SQLHelper(this);
        sqLiteDatabase = sqlHelper.getReadableDatabase();

        tv_account_name.setText(account_name);
        tv_account_number.setText(Method.space_4(account_number));
    }

    public void set_adapter(){
        customAdapterReport = new CustomAdapterReport(this, objectReports);
        lv_report.setAdapter(customAdapterReport);

        new getDataTrans(ClientMutation.this.getApplicationContext()).execute("");
    }

    @SuppressLint("StaticFieldLeak")
    private class getDataTrans extends AsyncTask<String, String, String> {

        Context context;
        String excptn, code, message;

        getDataTrans(Context context)
        {
            this.context =  context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {

            try {
                sqLiteDatabase = sqlHelper.getReadableDatabase();
                cursor = sqLiteDatabase.rawQuery("SELECT * FROM transaction_client " +
                        "WHERE customer_id="+customer_id+" ORDER BY datetime(date_time) DESC", null);
                cursor.moveToFirst();

                if(cursor.getCount() > 0) {
                    code = "200";
                    message = "OK";

                    objectReports.clear();
                   for(int i=0; i<cursor.getCount(); i++){
                       cursor.moveToPosition(i);
                       objectReports.add(new ObjectReport(cursor.getString(4), cursor.getString(5), cursor.getString(1)));
                   }
                }else{
                    code = "500";
                    message = "Data Not Found";
                }

                excptn = "good";
            } catch (NullPointerException e){
                e.printStackTrace();
                excptn = "error";
            }
            return "";
        }

        @SuppressLint("SetTextI18n")
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(excptn.equals("error")){
                new MaterialDialog.Builder(ClientMutation.this)
                        .title("Internal Error!")
                        .content("Internal Error, Please try again")
                        .show();
            }else {
                if(!code.equals("200")){
                    new MaterialDialog.Builder(ClientMutation.this)
                            .title("Sorry!")
                            .content(message)
                            .show();
                }else{
                    customAdapterReport.notifyDataSetChanged();
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(ClientMutation.this, ClientHome.class);
        startActivity(i);
    }
}
