package project.rhiedzal.bammsbankproject.CLIENT;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import project.rhiedzal.bammsbankproject.CLIENT.MUTATION.ClientMutation;
import project.rhiedzal.bammsbankproject.CLIENT.TRANSFER.ClientTransfer;
import project.rhiedzal.bammsbankproject.CLIENT.WITHDRAW.ClientWithdraw;
import project.rhiedzal.bammsbankproject.Home;
import project.rhiedzal.bammsbankproject.METHOD.Method;
import project.rhiedzal.bammsbankproject.R;
import project.rhiedzal.bammsbankproject.SQL.SQLHelper;
import project.rhiedzal.bammsbankproject.SQL.SessionLogin;

public class ClientHome extends AppCompatActivity {

    @BindView(R.id.account_name) TextView tv_account_name;
    @BindView(R.id.account_amount) TextView tv_account_amount;
    @BindView(R.id.account_number) TextView tv_account_number;
    @BindView(R.id.deposite) Button btn_deposite;
    @BindView(R.id.withdraw) Button btn_withdraw;
    @BindView(R.id.transfer) Button btn_transfer;
    @BindView(R.id.mutation) Button btn_mutation;

    Cursor cursor;
    SQLHelper sqlHelper;
    SQLiteDatabase sqLiteDatabase;

    HashMap<String, String> login;
    String id_customer, account_money, account_number, account_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_home);
        ButterKnife.bind(this);

        declare_function();
        click_action();
    }

    public void declare_function(){
        login = new SessionLogin(getApplicationContext()).getLoginDetail();
        id_customer = login.get(SessionLogin.KEY_ID_CLIENT);

        sqlHelper = new SQLHelper(this);
        sqLiteDatabase = sqlHelper.getReadableDatabase();

        new getData(ClientHome.this.getBaseContext()).execute("");
    }

    public void click_action(){
        btn_deposite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new MaterialDialog.Builder(ClientHome.this)
                        .title("Input Deposit Amount")
                        .inputType(InputType.TYPE_CLASS_NUMBER)
                        .input("0", "", new MaterialDialog.InputCallback() {
                            @Override
                            public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                                dialog.dismiss();
                                if(TextUtils.isEmpty(input)){
                                    new MaterialDialog.Builder(ClientHome.this)
                                            .title("Sorry!")
                                            .content("Input can't be empty")
                                            .show();
                                }else {
                                    new deposit(ClientHome.this.getBaseContext(), String.valueOf(input)).execute("");
                                }
                            }
                        })
                        .negativeText("Cancel")
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
            }
        });

        btn_withdraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ClientHome.this, ClientWithdraw.class);
                i.putExtra("customer_id", id_customer);
                i.putExtra("account_number", account_number);
                i.putExtra("account_amount", account_money);
                i.putExtra("account_name", account_name);
                startActivity(i);
            }
        });

        btn_transfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ClientHome.this, ClientTransfer.class);
                i.putExtra("customer_id", id_customer);
                i.putExtra("account_number", account_number);
                i.putExtra("account_amount", account_money);
                i.putExtra("account_name", account_name);
                startActivity(i);
            }
        });

        btn_mutation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ClientHome.this, ClientMutation.class);
                i.putExtra("customer_id", id_customer);
                i.putExtra("account_number", account_number);
                i.putExtra("account_amount", account_money);
                i.putExtra("account_name", account_name);
                startActivity(i);
            }
        });
    }

    @SuppressLint("StaticFieldLeak")
    private class getData extends AsyncTask<String, String, String> {

        Context context;
        String excptn, code, message;

        getData(Context context)
        {
            this.context =  context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {

            try {
                sqLiteDatabase = sqlHelper.getReadableDatabase();
                cursor = sqLiteDatabase.rawQuery("SELECT * FROM customer INNER JOIN account ON customer.customer_id = account.account_id " +
                        "WHERE customer.customer_id="+id_customer, null);
                cursor.moveToFirst();

                if(cursor.getCount() > 0) {
                    code = "200";
                    message = "OK";

                    account_money = cursor.getString(10);
                    account_number = cursor.getString(7);
                    account_name = cursor.getString(1);
                }else{
                    code = "500";
                    message = "Data Not Found";
                }

                excptn = "good";
            } catch (NullPointerException e){
                e.printStackTrace();
                excptn = "error";
            }
            return "";
        }

        @SuppressLint("SetTextI18n")
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(excptn.equals("error")){
                new MaterialDialog.Builder(ClientHome.this)
                        .title("Internal Error!")
                        .content("Internal Error, Please try again")
                        .show();
            }else {
                if(!code.equals("200")){
                    new MaterialDialog.Builder(ClientHome.this)
                            .title("Sorry!")
                            .content(message)
                            .show();
                }else{
                    tv_account_name.setText(account_name);
                    tv_account_amount.setText("Rp "+Method.titik(account_money));
                    tv_account_number.setText(Method.space_4(account_number));
                }
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class deposit extends AsyncTask<String, String, String> {

        Context context;
        String nominal, excptn;
        int money;

        deposit(Context context, String nominal)
        {
            this.context =  context;
            this.nominal = nominal;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @SuppressLint("SimpleDateFormat")
        @Override
        protected String doInBackground(String... params) {

            try {
                sqLiteDatabase = sqlHelper.getReadableDatabase();
                cursor = sqLiteDatabase.rawQuery("SELECT amount FROM account WHERE account_id="+id_customer, null);
                cursor.moveToFirst();
                money = cursor.getInt(0);

                //update amount
                sqLiteDatabase = sqlHelper.getWritableDatabase();
                ContentValues cv = new ContentValues();
                cv.put("amount", String.valueOf(money + Integer.parseInt(nominal)));
                sqLiteDatabase.update("account", cv, "account_id="+id_customer, null);

                //insert data transaction
                ContentValues val_trans = new ContentValues();
                val_trans.put("type", "DEPOSITE");
                val_trans.put("name", account_name);
                val_trans.put("customer_id", id_customer);
                val_trans.put("amount", nominal);
                val_trans.put("date_time", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
                sqLiteDatabase.insert("transaction_client", null, val_trans);

                excptn = "good";
            } catch (NullPointerException e){
                e.printStackTrace();
                excptn = "error";
            }
            return "";
        }

        @SuppressLint("SetTextI18n")
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(excptn.equals("error")){
                new MaterialDialog.Builder(ClientHome.this)
                        .title("Internal Error!")
                        .content("Internal Error, Please try again")
                        .show();
            }else {
                new MaterialDialog.Builder(ClientHome.this)
                        .title("Deposit Success")
                        .positiveText("OK")
                        .cancelable(false)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                account_money = String.valueOf((Integer.parseInt(nominal) + Integer.parseInt(account_money)));
                                tv_account_amount.setText("Rp "+Method.titik(String.valueOf(money + Integer.parseInt(nominal))));
                            }
                        })
                        .show();
            }
        }
    }

    @Override
    public void onBackPressed() {
        new MaterialDialog.Builder(this)
                .title("Logout From Your Account?")
                .positiveText("YES")
                .negativeText("NO")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        Intent i = new Intent(ClientHome.this, Home.class);
                        startActivity(i);
                    }
                })
                .show();
    }
}
