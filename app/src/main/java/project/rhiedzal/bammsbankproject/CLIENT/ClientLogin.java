package project.rhiedzal.bammsbankproject.CLIENT;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import project.rhiedzal.bammsbankproject.Home;
import project.rhiedzal.bammsbankproject.R;
import project.rhiedzal.bammsbankproject.SQL.SQLHelper;
import project.rhiedzal.bammsbankproject.SQL.SessionLogin;

public class ClientLogin extends AppCompatActivity {

    @BindView(R.id.username) EditText et_username;
    @BindView(R.id.password) EditText et_password;
    @BindView(R.id.btn_login) Button btn_login;
    @BindView(R.id.register) TextView tv_register;

    String username, password;

    Cursor cursor;
    SQLHelper sqlHelper;
    SQLiteDatabase sqLiteDatabase;

    SessionLogin sessionLogin;
    HashMap<String, String> login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_login);
        ButterKnife.bind(this);

        declare_function();
        click_action();
    }

    public void declare_function(){
        sqlHelper = new SQLHelper(this);
        sqLiteDatabase = sqlHelper.getReadableDatabase();

        sessionLogin = new SessionLogin(getApplicationContext());
        login = sessionLogin.getLoginDetail();
    }

    public void click_action(){
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                username = et_username.getText().toString();
                password = et_password.getText().toString();

                if(TextUtils.isEmpty(username)){
                    et_username.setError("Can't be empty");
                }else if(TextUtils.isEmpty(password)){
                    et_password.setError("Can't be empty");
                }else{
                    new LoginCheck(ClientLogin.this.getBaseContext()).execute("");
                }
            }
        });

        tv_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ClientLogin.this, ClientRegister.class);
                startActivity(i);
            }
        });
    }

    @SuppressLint("StaticFieldLeak")
    private class LoginCheck extends AsyncTask<String, String, String> {

        Context context;
        String excptn, stat, cust_id;

        LoginCheck(Context context)
        {
            this.context =  context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {

            try {
                cursor = sqLiteDatabase.rawQuery("SELECT * FROM customer WHERE username='"+username+"' AND password='"+password+"'", null);
                cursor.moveToFirst();

                if(cursor.getCount() > 0){
                    cust_id = cursor.getString(0);
                    stat = "OK";
                }else{
                    stat = "NO";
                }

                excptn = "good";
            } catch (NullPointerException e){
                e.printStackTrace();
                excptn = "error";
            }
            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(excptn.equals("error")){
                new MaterialDialog.Builder(ClientLogin.this)
                        .title("Internal Error!")
                        .content("Internal Error, Please try again")
                        .show();
            }else {
                if(stat.equals("NO")){
                    new MaterialDialog.Builder(ClientLogin.this)
                            .title("Sorry!")
                            .content("Account not found")
                            .show();
                }else{
                    sessionLogin.createLoginSession(cust_id);
                    Intent i = new Intent(ClientLogin.this, ClientHome.class);
                    startActivity(i);
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(ClientLogin.this, Home.class);
        startActivity(i);
    }
}
