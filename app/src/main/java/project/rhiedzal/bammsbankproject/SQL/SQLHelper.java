package project.rhiedzal.bammsbankproject.SQL;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Rhiedzal on 7/17/2018.
 */

public class SQLHelper extends SQLiteOpenHelper {

    public static final String db_name = "SQL_BAMMS";
    public static final int db_version = 1;

    public SQLHelper(Context context){
        super(context, db_name, null, db_version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String create_customer = "create table customer(customer_id integer primary key, name text null, phone text null, address text null, username text null, password text null);";
        String create_account = "create table account(account_id integer primary key, account_number text null, type text null, description text null, amount text null);";
        String create_transaction = "create table transaction_client(transaction_id integer primary key, type text null, name text null, customer_id integer null, amount text null, date_time datetime null);";

        db.execSQL(create_customer);
        db.execSQL(create_account);
        db.execSQL(create_transaction);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
