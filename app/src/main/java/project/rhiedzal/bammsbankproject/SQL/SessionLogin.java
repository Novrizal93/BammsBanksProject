package project.rhiedzal.bammsbankproject.SQL;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;

/**
 * Created by Rhiedzal on 8/19/2018.
 */

public class SessionLogin {
    SharedPreferences pref;

    SharedPreferences.Editor editor;
    Context _context;
    int PRIVATE_MODE = 0;
    private static final String PREF_NAME = "login";

    public static final String KEY_ID_CLIENT = "id_client";

    public SessionLogin(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void createLoginSession(String id_client){

        editor.putString(KEY_ID_CLIENT, id_client);
        editor.commit();
    }

    public HashMap<String, String> getLoginDetail(){
        HashMap<String, String> login = new HashMap<>();

        login.put(KEY_ID_CLIENT, pref.getString(KEY_ID_CLIENT, null));
        return login;
    }

    public void bersihkan(){
        editor.clear();
        editor.commit();
    }
}