package project.rhiedzal.bammsbankproject;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import project.rhiedzal.bammsbankproject.CLIENT.ClientLogin;
import project.rhiedzal.bammsbankproject.MANAGER.ManagerLogin;

public class Home extends AppCompatActivity {

    @BindView(R.id.client) Button btn_client;
    @BindView(R.id.bank_manager) Button btn_bank_manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        ButterKnife.bind(this);

        try {
            InputStream is = getAssets().open("JsonLoginManager");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            Log.d("popo", "isi :"+new String(buffer));
        } catch (IOException e) {
            e.printStackTrace();
        }

        click_action();
    }

    public void click_action(){
        btn_client.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Home.this, ClientLogin.class);
                startActivity(i);
            }
        });

        btn_bank_manager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Home.this, ManagerLogin.class);
                startActivity(i);
            }
        });
    }

    @Override
    public void onBackPressed() {
        new MaterialDialog.Builder(this)
                .title("Close App?")
                .positiveText("YES")
                .negativeText("NO")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        finishAffinity();
                    }
                })
                .show();

    }
}
